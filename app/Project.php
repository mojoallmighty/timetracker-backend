<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Task;
use App\User;
use App\Hour;

class Project extends Model
{
    protected $fillable = ['name', 'description', 'created_by', 'create_date'];

    public static function get_projects($user_id) {
        $return = array();
        $projects = Project::where(
            ['created_by' => $user_id]
        )->get();

        foreach($projects as $project) {
            $project['tasks'] = Task::where(
                ['project_id' => $project->id]
            )->get();
            $user_first_name = User::find($project->created_by)->first_name;
            $user_last_name = User::find($project->created_by)->last_name;

            $project['created_by'] = ($project->created_by != $user_id) ? $user_first_name.' '.$user_last_name 
            : 'You';

            $hours = Hour::where(
                ['project_id' => $project->id]
            )->get();

            $project['people_involved'] = array();
            $involved = array();

            foreach($hours as $hour) {
                $user_name = User::find($hour->user_id)->first_name. ' ' .User::find($hour->user_id)->last_name;
                if(!in_array($user_name, $involved))
                    array_push($involved, $user_name);
            }
            $project['people_involved'] = $involved;

            array_push($return, $project);
        }

        return $return;
    }

    public static function get_all_projects($user_id) {
        $return = array();
        $projects = Project::all();

        foreach($projects as $project) {
            $project['tasks'] = Task::where(
                ['project_id' => $project->id]
            )->get();
            $user_first_name = User::find($project->created_by)->first_name;
            $user_last_name = User::find($project->created_by)->last_name;

            $project['created_by'] = ($project->created_by != $user_id) ? $user_first_name.' '.$user_last_name 
            : 'You';

            $hours = Hour::where(
                ['project_id' => $project->id]
            )->get();

            $project['people_involved'] = array();
            $involved = array();

            foreach($hours as $hour) {
                $user_name = User::find($hour->user_id)->first_name. ' ' .User::find($hour->user_id)->last_name;
                if(!in_array($user_name, $involved))
                    array_push($involved, $user_name);
            }
            $project['people_involved'] = $involved;

            array_push($return, $project);
        }

        return $return;
    }

}
