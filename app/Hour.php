<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    protected $fillable = ['task_id', 'project_id', 'user_id', 'hours', 'description', 'create_date'];
}
