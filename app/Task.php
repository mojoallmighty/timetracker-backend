<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['project_id', 'name', 'description', 'created_by', 'create_date'];

    public static function get_tasks($user_id) {
        return Task::where(
            ['created_by' => $user_id]
        )->get();
    }
}
