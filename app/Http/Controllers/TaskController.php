<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hour;
use App\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Retrieves a task by id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_task_by_id(Request $request) {
        $return = array();
        $workedHours = 0;
        $involvedPeople = 0;
        $personalWorkedHours = 0;

        $task = Task::find($request->all()['id'])->first();
        $hours = Hour::where(
            ['task_id' => $request->all()['id']]
        )->get();

        foreach($hours as $hour) {
            $workedHours += $hour->hours;
            $involvedPeople++;
        }

        $personalHours = Hour::where(
            ['task_id' => $request->all()['id'],
            'user_id' => $request->all()['user_session']]
        )->get();

        foreach($personalHours as $personalHour) {
            $personalWorkedHours += $personalHour->hours;
            array_push($return, array(
                'date' => date('d/m/Y', $personalHour->create_date),
                'description' => $personalHour->description,
                'hours' => $personalHour->hours
            ));
        }

        return array(
            'task_name' => $task->name,
            'total_worked_hours' => $workedHours,
            'personal_worked_hours' => $personalWorkedHours,
            'involved_people' => $involvedPeople,
            'task' => $return
        );
    }

    /**
     * Retrieves the tasks added by a certain user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_tasks(Request $request) {
        $response = !empty($request->all()['user_session']) ? Task::get_tasks($request->all()['user_session']) : '';

        return !empty($response) ?
        json_encode($response)
        : '';
    }

    /**
     * Retrieves all tasks
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_all_tasks(Request $request) {
        return Task::all();
    }

    /**
     * Adds a task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_task(Request $request) {
        $req = !empty($request->all()) ? 
        array(
            'project_id' => !empty($request->all()['project_id']) ? $request->all()['project_id'] : '',
            'name' => !empty($request->all()['task_name']) ? $request->all()['task_name'] : '',
            'description' => !empty($request->all()['task_description']) ? $request->all()['task_description'] : '',
            'created_by' => !empty($request->all()['user_session']) ? $request->all()['user_session'] : '',
            'create_date' => time()
        ) : '';

        $id = Task::create($req)->id;

        return !empty($id) ? json_encode('ok') :
        json_encode(array('error' => 'There was an error adding the project'));
    }
}
