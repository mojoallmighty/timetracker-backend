<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{

    private $passwordKey = 'rankingCoach';
    private $passwordMethod = 'bf-ecb';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return User::all();
        return 'works';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Checks the email for login.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request) {
        return !empty($request->all()['username']) ? 
        (!empty(User::check_username($request->all()['username'])) ? json_encode(array('status' => 'ok')) : json_encode(array('error' => 'login'))) 
        : json_encode(array('error' => 'login'));
    }

    /**
     * Login function.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        $response = !empty($request->all()['email']) && !empty($request->all()['password']) ? 
        User::login($request->all()['email'], $this->encrypt_password($request->all()['password'])) : '';
        
        return !empty($response) ? 
        json_encode(array(
            'user_first_name' => $response['first_name'],
            'user_last_name' => $response['last_name'],
            'user_session' => $response['id'],
            'token' => ''
        ))
        : json_encode(array('error' => 'login'));
    }

    /**
     * Passowrd encryption function.
     *
     * @param  string  $password
     * @return string
     */
    public function encrypt_password($password) {
        return base64_encode(openssl_encrypt($password, $this->passwordMethod, $this->passwordKey));
    }

    /**
     * Passowrd decryption function.
     *
     * @param  string  $password
     * @return string
     */
    public function decrypt_password($password) {
        return openssl_decrypt(base64_decode($password), $this->passwordMethod, $this->passwordKey);
    }
}
