<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task;
use App\Hour;

class HourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Adds hours for a task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_hours(Request $request) {
        $req = !empty($request->all()) ? 
        array(
            'task_id' => !empty($request->all()['task_id']) ? $request->all()['task_id']: '',
            'project_id' => !empty($request->all()['task_id']) ? 
                Task::where(
                    ['id' => $request->all()['task_id']]
                )->first()->project_id
            : '',
            'hours' => !empty($request->all()['hours']) ? $request->all()['hours'] : '',
            'description' => !empty($request->all()['description']) ? $request->all()['description'] : '',
            'user_id' => !empty($request->all()['user_session']) ? $request->all()['user_session'] : '',
            'create_date' => time()
        ) : '';

        $id = Hour::create($req)->id;
        return !empty($id) ? json_encode('ok') :
        json_encode(array('error' => 'There was an error adding the project'));
    }
}
