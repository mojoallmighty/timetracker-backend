<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Project;
use App\Task;
use App\Hour;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Retrieves the projects added by a certain user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_projects(Request $request) {
        $response = !empty($request->all()['user_session']) ? Project::get_projects($request->all()['user_session']) : '';

        return !empty($response) ?
        json_encode($response)
        : '';
    }

    /**
     * Retrieves all projects
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_all_projects(Request $request) {
        $response = !empty($request->all()['user_session']) ? Project::get_all_projects($request->all()['user_session']) : '';

        return !empty($response) ?
        json_encode($response)
        : '';
    }

    /**
     * Retrieves all projects' id and names for select box
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_all_projects_for_select(Request $request) {
        $return = '';
        $projects = Project::all();

        foreach($projects as $project) {
            $return[$project->id]['value'] = $project->id;
            $return[$project->id]['name'] = $project->name;
        }

        return !empty($return) ?
        json_encode($return)
        : '';
    }

    /**
     * Get weekly feed for homepage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_week_homepage(Request $request) {
        $totalHours = 0;
        $tasksArray = array();
        $projectsArray = array();

        $hours = Hour::where(
            ['user_id' => $request->all()['user_session']]
        )
        ->having('create_date', '>=', mktime(0, 0, 0, date("n"), date("j") - date("N") + 1)) //this week
        ->get();
        
        foreach($hours as $hour) {
            $totalHours ++;
            if(!in_array($hour->task_id, $tasksArray))
                array_push($tasksArray, $hour->task_id);
            if(!in_array($hour->project_id, $projectsArray))
                array_push($projectsArray, $hour->project_id);
        }

        return array(
            'hours' => $totalHours,
            'tasks' => count($tasksArray),
            'projects' => count($projectsArray)
        );
        
    }

    /**
     * Get monthly feed for homepage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_month_homepage(Request $request) {
        $totalHours = 0;
        $tasksArray = array();
        $projectsArray = array();

        $hours = Hour::where(
            ['user_id' => $request->all()['user_session']]
        )
        ->having('create_date', '>=', strtotime(date('Y-m-01 00:00:00'))) //this month
        ->get();
        
        foreach($hours as $hour) {
            $totalHours ++;
            if(!in_array($hour->task_id, $tasksArray))
                array_push($tasksArray, $hour->task_id);
            if(!in_array($hour->project_id, $projectsArray))
                array_push($projectsArray, $hour->project_id);
        }

        return array(
            'hours' => $totalHours,
            'tasks' => count($tasksArray),
            'projects' => count($projectsArray)
        );
    }

    /**
     * Adds a project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add_project(Request $request) {
        $req = !empty($request->all()) ? 
        array(
            'name' => !empty($request->all()['project_name']) ? $request->all()['project_name'] : '',
            'description' => !empty($request->all()['project_description']) ? $request->all()['project_description'] : '',
            'created_by' => !empty($request->all()['user_session']) ? $request->all()['user_session'] : '',
            'create_date' => time()
        ) : '';

        $id = Project::create($req)->id;
        $task_id = !empty($id) && !empty($request->all()['task_name']) && !empty($request->all()['task_description']) ? 
        Task::create(
            array(
                'project_id' => $id,
                'name' => $request->all()['task_name'],
                'description' => $request->all()['task_description'],
                'created_by' => !empty($request->all()['user_session']) ? $request->all()['user_session'] : '',
                'create_date' => time()
            )
        )->id : '';

        return !empty($id) ? json_encode('ok') :
        json_encode(array('error' => 'There was an error adding the project'));
    }
}
