<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(array('prefix' => 'api/'), function() {
    Route::post('check-username',['middleware' => 'cors' , 'uses'=>  'UserController@check'])->middleware('cors');
    Route::post('login',['middleware' => 'cors' , 'uses'=>  'UserController@login'])->middleware('cors');
    Route::post('check-token',['middleware' => 'cors' , 'uses'=>  'UserController@check_token'])->middleware('cors');
    Route::post('get-week-homepage',['middleware' => 'cors' , 'uses'=>  'ProjectController@get_week_homepage'])->middleware('cors');
    Route::post('get-month-homepage',['middleware' => 'cors' , 'uses'=>  'ProjectController@get_month_homepage'])->middleware('cors');
    Route::post('get-projects',['middleware' => 'cors' , 'uses'=>  'ProjectController@get_projects'])->middleware('cors');
    Route::post('get-all-projects',['middleware' => 'cors' , 'uses'=>  'ProjectController@get_all_projects'])->middleware('cors');
    Route::post('get-all-projects-for-select',['middleware' => 'cors' , 'uses'=>  'ProjectController@get_all_projects_for_select'])->middleware('cors');
    Route::post('add-project',['middleware' => 'cors' , 'uses'=>  'ProjectController@add_project'])->middleware('cors');
    Route::post('get-task-by-id',['middleware' => 'cors' , 'uses'=>  'TaskController@get_task_by_id'])->middleware('cors');
    Route::post('get-all-tasks',['middleware' => 'cors' , 'uses'=>  'TaskController@get_all_tasks'])->middleware('cors');
    Route::post('get-tasks',['middleware' => 'cors' , 'uses'=>  'TaskController@get_tasks'])->middleware('cors');
    Route::post('add-task',['middleware' => 'cors' , 'uses'=>  'TaskController@add_task'])->middleware('cors');
    Route::post('add-hour',['middleware' => 'cors' , 'uses'=>  'HourController@add_hours'])->middleware('cors');
});
